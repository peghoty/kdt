#========================================================================#
#  KDT (K-d tree) (c) 2013-2014                                          #
#  peghoty@163.com     (Zhou Zhiyang)                                    #
#========================================================================#

include ./makefile.pub

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# tar files name
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TARFILE = `date +%Y.%m.%d.kdt.tar.gz`

#----------------------------------
# C source files directories
#----------------------------------
CSRC_DIR = ${KDT_DIR}/csrc
CSRC := $(foreach dir,$(CSRC_DIR),$(wildcard $(CSRC_DIR)/*.c))
OBJSC = $(patsubst %.c,%.o,$(CSRC))

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# construct a lib
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
${KDT_DIR}/lib/libKDT.a: $(OBJSC)
	ar rv $@ $^
	ranlib $@

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#  Clean all *.o and *~ in the current directory
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
clean :
	find ${KDT_DIR}/ -name "*.o" -delete
	find ${KDT_DIR}/ -name "*~" -delete

allclean:
	make clean
	rm -f ${KDT_DIR}/lib/libKDT.a

redo:
	make allclean
	make
	make clean
	

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# back up the KDT package
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
tar:	
	tar zcvf ./${TARFILE} ../kdt/*

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# get tags
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++      
ctags:
	-ctags -R --c-kinds=+p --fields=+S

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# count the total number of lines
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 	
linecount:
	find ${KDT_DIR} -type f -print0 | xargs -0 wc -l

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# help information
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
help:
	@echo " "
	@echo " %----------------------------------------------------------% "
	@echo " |                     KDT package                          | " 
	@echo " %----------------------------------------------------------% "
	@echo " "
	@echo " make          : build all exe files and a library "
	@echo " make clean    : clean all obj, exe, bak, out files "
	@echo " make allclean : clean all obj, exe, bak, out and lib files "
	@echo " make redo     : make allclean; make; make clean "
	@echo " make tar      : back up the current KDT package "
	@echo " make help     : show this screen "
	@echo " "		
