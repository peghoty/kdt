/*========================================================================*
 *  KDT (K-d tree analysis) (c) 2013-2014                                 *
 *  peghoty@163.com     (Zhou Zhiyang)                                    *
 *========================================================================*/
 
#ifndef KDT_UTL_HEADER
#define KDT_UTL_HEADER

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <ctype.h>
#include <assert.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>


/*----------------------------------------------------------------*
 *                      Macro Definition                          *
 *----------------------------------------------------------------*/

/* time testing */  
#define kdt_gettime(a) gettimeofday(&a,NULL)
#define kdt_mytime(a,b) ((b.tv_sec-a.tv_sec) + (float)(b.tv_usec-a.tv_usec)/1000000.0) 

/* max and min operation */
#define kdt_max(a,b)  (((a)<(b)) ? (b) : (a))
#define kdt_min(a,b)  (((a)<(b)) ? (a) : (b))

/* memory */
#define kdt_malloc(type, count)       ( (type *)kdt_Malloc((size_t)(sizeof(type) * (count))) )
#define kdt_calloc(type, count)       ( (type *)kdt_Calloc((size_t)(count), (size_t)sizeof(type)) )
#define kdt_realloc(ptr, type, count) ( (type *)kdt_Realloc((char *)ptr, (size_t)(sizeof(type) * (count))) )
#define kdt_free(ptr)                 ( kdt_Free((char *)ptr), ptr = NULL )

/*----------------------------------------------------------------*
 *                   Function Declaration                         *
 *----------------------------------------------------------------*/

/* csrc/error.c */
void kdt_error_handler( char *filename, int line, int ierr );
#define kdt_error(IERR)  kdt_error_handler(__FILE__, __LINE__, IERR)
#define kdt_assert(EX) if(!(EX)) {fprintf(stderr,"kdt_assert failed: %s\n", #EX); kdt_error(1); exit(0);}

/* csrc/memory.c */
int   kdt_OutOfMemory( size_t size );
char *kdt_Malloc( size_t size );
char *kdt_Calloc( size_t count, size_t elt_size );
char *kdt_Realloc( char *ptr, size_t size );
void  kdt_Free( char *ptr );


#endif
