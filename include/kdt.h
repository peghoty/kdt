/*========================================================================*
 *  KDT (K-d tree analysis) (c) 2013-2014                                 *
 *  peghoty@163.com     (Zhou Zhiyang)                                    *
 *========================================================================*/
 
#ifndef KDT_HEADER
#define KDT_HEADER

#ifdef __cplusplus
extern "C" {
#endif

#ifndef KDT_UTL_HEADER
#include "utl.h"
#endif

/*----------------------------------------------------------------*
 *                      Macro Definition                          *
 *----------------------------------------------------------------*/
 
#define SQ(x)  ((x) * (x))   // 平方的宏定义

/*----------------------------------------------------------------*
 *                     Struct Declaration                         *
 *----------------------------------------------------------------*/
  
// 超矩形结构体(包含 kd 树中所有节点的最小超矩形)    
struct kdhyperrect 
{
   int     dim;  // 超矩形的维数
   double *min;  // min[i] 表示第 i 个维度上的最小值, i = 0(1)dim-1
   double *max;  // min[i] 表示第 i 个维度上的最大值, i = 0(1)dim-1
};

// kd 树节点结构体
struct kdnode 
{
   double *pos;   // 节点对应的向量(长度为 dim) 
   int     dir;   // direction, 分裂方向
   void   *data;  

   struct kdnode *left;  // 左子树
   struct kdnode *right; // 右子树
};

// 查询结果节点结构体(单链表的形式) 
struct res_node 
{
   struct kdnode   *item;    // 指向查询结果当前节点对应的 kd 树节点的指针
   double           dist_sq; // 当前节点对应的向量和基准向量的距离(的平方)或 -1
   struct res_node *next;    // 下一个查询结果节点的指针
};

// kd 树结构体 
struct kdtree 
{
   int                  dim;    // 维数
   struct kdnode       *root;   // 根节点
   struct kdhyperrect  *rect;   // 超矩形
   void (*destr)(void*);        // 销毁函数(默认为空, 可由用户定义)
};

// 查询结果结构体
struct kdres 
{
   struct kdtree    *tree;   // kd 树
   struct res_node  *rlist;  // 存放查询结果的所有节点及对应的距离值
   struct res_node  *riter;  // 辅助指针
   int               size;   // 查询结果链表的长度(即满足查询条件的节点的个数)
};


/*----------------------------------------------------------------*
 *                   Function Declaration                         *
 *----------------------------------------------------------------*/
 
/* create a kd-tree for "k"-dimensional data */
struct kdtree *kd_create(int k);

/* free the struct kdtree */
void kd_free(struct kdtree *tree);

/* remove all the elements from the tree */
void kd_clear(struct kdtree *tree);

/* if called with non-null 2nd argument, the function provided
 * will be called on data pointers (see kd_insert) when nodes
 * are to be removed from the tree.
 */
void kd_data_destructor(struct kdtree *tree, void (*destr)(void*));

/* insert a node, specifying its position, and optional data */
int kd_insert(struct kdtree *tree, const double *pos, void *data);

/* Find the nearest node from a given point.
 *
 * This function returns a pointer to a result set with at most one element.
 */
struct kdres *kd_nearest(struct kdtree *tree, const double *pos);

/* Find any nearest nodes from a given point within a range.
 *
 * This function returns a pointer to a result set, which can be manipulated
 * by the kd_res_* functions.
 * The returned pointer can be null as an indication of an error. Otherwise
 * a valid result set is always returned which may contain 0 or more elements.
 * The result set must be deallocated with kd_res_free after use.
 */
struct kdres *kd_nearest_range(struct kdtree *tree, const double *pos, double range);

/* frees a result set returned by kd_nearest_range() */
void kd_res_free(struct kdres *set);

/* returns the size of the result set (in elements) */
int kd_res_size(struct kdres *set);

/* rewinds the result set iterator */
void kd_res_rewind(struct kdres *set);

/* returns non-zero if the set iterator reached the end after the last element */
int kd_res_end(struct kdres *set);

/* advances the result set iterator, returns non-zero on success, zero if
 * there are no more elements in the result set.
 */
int kd_res_next(struct kdres *set);

/* returns the data pointer (can be null) of the current result set item
 * and optionally sets its position to the pointers(s) if not null.
 */
void *kd_res_item(struct kdres *set, double *pos);

/* equivalent to kd_res_item(set, 0) */
void *kd_res_item_data(struct kdres *set);

/* Find the nearest node from a given point.
 *
 * This function returns a pointer to a result set with at most one element.
 */
struct kdres *kd_nearest(struct kdtree *tree, const double *pos);

#ifdef __cplusplus
}
#endif

#endif 
 
