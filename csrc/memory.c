/*========================================================================*
 *  KDT (K-d tree analysis) (c) 2013-2014                                 *
 *  peghoty@163.com     (Zhou Zhiyang)                                    *
 *========================================================================*/

#include "utl.h"

/*!
 * \fn int kdt_OutOfMemory
 * \brief Out-of-memory warning.
 * \date 2013/07/31 
 */  
int
kdt_OutOfMemory( size_t size )
{
   printf("Out of memory trying to allocate %d bytes\n", (int) size);
   fflush(stdout);
   kdt_error(-1);
   return 0;
}

/*!
 * \fn char *kdt_Malloc
 * \brief Allocate memory.
 * \date 2013/07/31 
 */  
char *
kdt_Malloc( size_t size )
{
   char *ptr;

   if (size > 0)
   {
      ptr = malloc(size);
      if (ptr == NULL)
      {
         kdt_OutOfMemory(size);
      }
   }
   else
   {
      ptr = NULL;
   }

   return ptr;
}

/*!
 * \fn char *kdt_Calloc
 * \brief Allocate memory.
 * \date 2013/07/31 
 */ 
char *
kdt_Calloc( size_t count, size_t elt_size )
{
   char *ptr;
   size_t size = count*elt_size;
   
   if (size > 0)
   {
      ptr = calloc(count, elt_size);
      if (ptr == NULL)
      {
         kdt_OutOfMemory(size);
      }     
   }
   else
   {
      ptr = NULL;
   }

   return ptr;
}

/*!
 * \fn char *kdt_Realloc
 * \brief Reallocate memory.
 * \date 2013/07/31 
 */ 
char *
kdt_Realloc( char *ptr, size_t size )
{
   if (ptr == NULL)
   {
      ptr = malloc(size);
   }
   else
   {
      ptr = realloc(ptr, size);
   }

   if ((ptr == NULL) && (size > 0))
   {
      kdt_OutOfMemory(size);
   }

   return ptr;
}

/*!
 * \fn void kdt_Free
 * \brief Free memory.
 * \date 2013/07/31 
 */ 
void
kdt_Free( char *ptr )
{
   if (ptr) free(ptr);
}
