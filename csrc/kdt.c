/*========================================================================*
 *  KDT (K-d tree analysis) (c) 2013-2014                                 *
 *  peghoty@163.com     (Zhou Zhiyang)                                    *
 *========================================================================*/
 
#include "kdt.h"

#if 0 // there exists some memory-leak problem, so don't turn on this for present.
#define USE_LIST_NODE_ALLOCATOR 1
#endif

static void clear_rec(struct kdnode *node, void (*destr)(void*));
static int insert_rec(struct kdnode **node, const double *pos, void *data, int dir, int dim);
static int rlist_insert(struct res_node *list, struct kdnode *item, double dist_sq);
static void clear_results(struct kdres *set);
static struct kdhyperrect* hyperrect_create(int dim, const double *min, const double *max);
static double hyperrect_dist_sq( struct kdhyperrect *rect, const double *pos );
static struct kdhyperrect* hyperrect_duplicate( const struct kdhyperrect *rect );
static void hyperrect_free(struct kdhyperrect *rect);
static void hyperrect_extend(struct kdhyperrect *rect, const double *pos);
static void kd_nearest_i(struct kdnode *node, const double *pos, struct kdnode **result, 
                         double *result_dist_sq, struct kdhyperrect* rect);

// special list node allocators
#ifdef USE_LIST_NODE_ALLOCATOR
static struct res_node *alloc_resnode(void);
static void free_resnode(struct res_node*);
static struct res_node *free_nodes;
static struct res_node *alloc_resnode( void )
{
   struct res_node *node;

   if (!free_nodes) 
   {
      node = malloc(sizeof *node);
   } 
   else 
   {
      node = free_nodes;
      free_nodes = free_nodes->next;
      node->next = 0;
   }

   return node;
}
static void free_resnode(struct res_node *node)
{
   node->next = free_nodes;
   free_nodes = node;
}
#else
#define alloc_resnode()  malloc(sizeof(struct res_node))
#define free_resnode(n)  free(n)
#endif

/*!
 * \fn hyperrect_create
 * \brief 为 kd 树创建一个超矩形, 该超矩形为可以包住数据集中的所有节点的体积最小的超矩形.
 * \param dim kd 树的维度.
 * \param min 长度为 dim 的数组, 超矩形每个维度的最小值.
 * \param max 长度为 dim 的数组, 超矩形每个维度的最大值.
 * \date 2013/07/31
 */
static struct kdhyperrect* hyperrect_create( int dim, const double *min, const double *max )
{
   size_t size = dim * sizeof(double);
   struct kdhyperrect* rect = 0;

   // 开设空间
   if (!(rect = malloc(sizeof(struct kdhyperrect)))) 
   {
      return 0;
   }

   rect->dim = dim;
   
   if (!(rect->min = malloc(size))) 
   {
      free(rect);
      return 0;
   }
	
   if (!(rect->max = malloc(size))) 
   {
      free(rect->min);
      free(rect);
      return 0;
   }
	
   // 首次创建，直接赋值即可
   memcpy(rect->min, min, size);
   memcpy(rect->max, max, size);

   return rect;
}

/*!
 * \fn hyperrect_extend
 * \brief 新增一个节点后, 刷新超矩形各个维度的最大值和最小值.
 * \param rect 指向超矩形的指针.
 * \param pos 新增节点对应的向量.
 * \date 2013/07/31
 */ 
static void hyperrect_extend( struct kdhyperrect *rect, const double *pos )
{
   int i;

   for (i = 0; i < rect->dim; i ++) 
   {
      if (pos[i] < rect->min[i]) 
      {
         rect->min[i] = pos[i];
      }
      if (pos[i] > rect->max[i]) 
      {
         rect->max[i] = pos[i];
      }
   }
}

/*!
 * \fn hyperrect_dist_sq
 * \date 2013/08/01
 */ 
static double hyperrect_dist_sq( struct kdhyperrect *rect, const double *pos )
{
   int i;
   double result = 0;

   for (i=0; i < rect->dim; i++) 
   {
      if (pos[i] < rect->min[i]) 
      {
         result += SQ(rect->min[i] - pos[i]);
      } 
      else if (pos[i] > rect->max[i]) 
      {
         result += SQ(rect->max[i] - pos[i]);
      }
   }

   return result;
}

/*!
 * \fn hyperrect_duplicate
 * \brief 复制一个超矩形.
 * \param rect 指向超矩形的指针.
 * \date 2013/08/01
 */ 
static struct kdhyperrect* hyperrect_duplicate( const struct kdhyperrect *rect )
{
   return hyperrect_create(rect->dim, rect->min, rect->max);
}

/*!
 * \fn hyperrect_free
 * \brief 释放一个超矩形.
 * \param rect 指向超矩形的指针.
 * \date 2013/08/01
 */ 
static void hyperrect_free( struct kdhyperrect *rect )
{
   free(rect->min);
   free(rect->max);
   free(rect);
}

/*!
 * \fn find_nearest
 * \brief 利用递归, 在 kd 树中查找与基准点 pos (欧氏)距离不超过 range 的所有节点, 
 *        获取结果链表和链表中的节点个数.
 * \param node kd 树中当前节点的指针.
 * \param pos 用于查找的基准向量.
 * \param range 范围阈值.
 * \param list 包含 kd 树中满足条件的所有节点的单链表.
 * \param ordered 标志变量(用于函数 rlist_insert 中).
 * \param dim kd 树的维度. 
 * \date 2013/07/31
 */
static int find_nearest( struct kdnode *node, const double *pos, double range, struct res_node *list, int ordered, int dim )
{
   double dist_sq, dx;
   int i, ret, added_res = 0;

   if (!node) // 已经查找到叶子结点, 返回 0
   {
      return 0;
   }
   
   dist_sq = 0;
   for (i = 0; i < dim; i ++) 
   {
      dist_sq += SQ(node->pos[i] - pos[i]); // 当前节点对应向量与基准向量的距离(的平方)
   }
   	
   if (dist_sq <= SQ(range)) // 若当前节点满足距离要求
   {
      // 将节点插入到返回结果的链表中 
      if (rlist_insert(list, node, ordered ? dist_sq : -1.0) == -1) 
      {
         return -1;
      }
      added_res = 1;
   }

   dx = pos[node->dir] - node->pos[node->dir];

   // 选择 node 的一边进行查找 (方向选取策略与建树时保持一致, 即相等的情形归在右边)
   // 作者原代码中第一个参数用的是 dx <= 0.0 ? node->left : node->right
   ret = find_nearest(dx < 0.0 ? node->left : node->right, pos, range, list, ordered, dim);
   
   // 若节点的另一边可能存在满足要求的点, 则继续查找
   // 作者原代码中的条件用的是 if (ret >= 0 && fabs(dx) < range)	
   if (ret >= 0 && fabs(dx) <= range) 
   {
      added_res += ret;
      // 作者原代码中第一个参数用的是 dx <= 0.0 ? node->left : node->right
      ret = find_nearest(dx < 0.0 ? node->right : node->left, pos, range, list, ordered, dim);
   }
	
   if (ret == -1) 
   {
      return -1;
   }
   added_res += ret;

   return added_res;
}

/*!
 * \fn rlist_insert
 * \brief 将一个新节点插入查询结果链表中.
 * \param list 指向查询结果链表表头的指针.
 * \param item 待插入链表的新节点.
 * \param dist_sq 取值为 -1 或 当前节点与基准节点的距离(的平方). 
 * \remark inserts the item. if dist_sq is >= 0, then do an ordered insert.
 * \TODO: make the ordering code use heapsort.
 * \date 2013/08/01
 */
static int rlist_insert( struct res_node *list, struct kdnode *item, double dist_sq )
{
   struct res_node *rnode;

   if ( !(rnode = alloc_resnode()) ) 
   {
      return -1;
   }
  
   rnode->item = item;
   
   // 注意: 当 find_nearest 中的参数 ordered 取 0 时, 
   // dist_sq 保存的是 -1 而不是真实距离(的平方)   
   rnode->dist_sq = dist_sq; 


   // 当 find_nearest 中的参数 ordered 取 1 时, 将新节点按升序插入链表中
   // 且 dist_sq 保存的是真实距离(的平方)
   if (dist_sq >= 0.0) 
   {
      while (list->next && list->next->dist_sq < dist_sq) 
      {
         list = list->next;
      }
   }

   rnode->next = list->next; // list->next 初始为空
   list->next  = rnode;
   
   // remark: 当 find_nearest 中的参数 ordered 取 0 时, 按照上述代码, 新增的节点
   // 将被插入在链表的前端, 即遍历 list 时, 最后加入的节点将被最先访问到.

   return 0;
}

/*!
 * \fn insert_rec
 * \brief 将新增节点插入到树的适当位置.
 * \param nptr 插入节点时需从树的根节点开始逐级往下遍历(去寻找合适位置), nptr 指向当前遍历到的节点.
 * \param pos 待插入节点对应的向量.
 * \param data
 * \param dir 分裂方向对应的下标
 * \param dim kd 树的维度 
 * \date 2013/07/31
 */
static int 
insert_rec( struct kdnode **nptr, const double *pos, void *data, int dir, int dim )
{
   int new_dir;
   struct kdnode *node;
         
   // 若当前节点为空，则利用 pos 建立一个新的 node
   if (!*nptr) 
   {
      // 为 node 结构体开设空间
      if (!(node = malloc(sizeof *node))) 
      {
         return -1;
      }
		
      // 为节点 node 对应的向量 pos 开设空间
      if (!(node->pos = malloc(dim * sizeof *node->pos))) 
      {
         free(node);
         return -1;
      }
		
      // 将 pos 数据拷贝到 node->pos 里面
      memcpy(node->pos, pos, dim * sizeof *node->pos);
      node->data  = data;
      node->dir   = dir;
      node->left  = 0;
      node->right = 0;
      
     *nptr = node;
      return 0;
   }

   node = *nptr; 
   
   // 分裂方向公式, 这里采用的是简单的轮换.
   // 一种更合理的做法是: 考察样本数据各分量的方差, 每次取方差最大的分量方向来做分裂,
   // 因为方差大说明沿该方向数据分得比较开, 沿这个方向进行分裂可获得最好的分辨率.
   // 当然这种方法要求一次性提供所有数据, 而本程序的方法建立 kd 树时不受该限制, 可以依次加入节点.
   new_dir = (node->dir + 1) % dim;    
	
   if (pos[node->dir] < node->pos[node->dir]) 
   {
      return insert_rec(&(*nptr)->left, pos, data, new_dir, dim);
   }
   
   // 注意: 两值相等的节点分到了右子树	
   return insert_rec(&(*nptr)->right, pos, data, new_dir, dim);
}

/*!
 * \fn kd_create 
 * \brief 创建一个数据维数为 k 的 kd 树, 返回一个指向新创建 kd 树的指针.
 * \date 2013/07/31
 */
struct kdtree *kd_create( int k )
{
   struct kdtree *tree;

   // 为 tree 开设空间
   if (!(tree = malloc(sizeof *tree))) 
   {
      return 0;
   }

   tree->dim   = k;   // 维度设置为 k
   tree->root  = 0;   // 根节点为 0
   tree->destr = 0;   // 销毁数据的函数
   tree->rect  = 0;   // 超矩形

   return tree;
}

/*!
 * \fn kd_insert 
 * \brief 在 kd 树 tree 中插入新节点，新节点对应的向量为 pos (长度可从 tree 的成员 dim 查询).
 * \param tree 待插入操作的 kd 树.
 * \param pos 待插入节点对应的向量.
 * \param data
 * \date 2013/07/31
 */
int kd_insert( struct kdtree *tree, const double *pos, void *data )
{
   // 将新增节点插入到树的适当位置
   // insert_rec 的正常返回值为 0
   if (insert_rec(&tree->root, pos, data, 0, tree->dim)) 
   {
      return -1;
   }
   
#if 1
   if (tree->rect == 0) // 如果树还没有超矩形, 则创建一个超矩形  
   {
      tree->rect = hyperrect_create(tree->dim, pos, pos);
   } 
   else // 如果已经有了超矩形, 则扩展原有的超矩形
   {
      hyperrect_extend(tree->rect, pos);
   }
#endif

   return 0;
}

/*!
 * \fn kd_nearest_range
 * \brief 在 kd 树中找出与 pos 的距离小于阀值 range 的所有节点.
 * \param kd 指向 kd 树的指针.
 * \param pos 用于查找的基准向量.
 * \param range 阀值参数.
 * \return 返回一个包含所有满足条件的节点构成的单链表.
 * \date 2013/07/31
 */
struct kdres *kd_nearest_range( struct kdtree *kd, const double *pos, double range )
{
   int ret;            // 查询结果的个数
   struct kdres *rset; // 查询结果指针
	
   // 初始化 rset
   if (!(rset = malloc(sizeof *rset))) // 开设空间
   {
      return 0;
   }
   if ( !(rset->rlist = alloc_resnode()) ) 
   {
      free(rset);
      return 0;
   }
   rset->rlist->next = 0;
   rset->tree = kd;
	
   // find_nearest 返回查询结果的个数 ret	
   if ( (ret = find_nearest(kd->root, pos, range, rset->rlist, 0, kd->dim)) == -1 ) 
   {
      kd_res_free(rset);
      return 0;
   }
   
   // 保存查询结果个数
   rset->size = ret;
   
   kd_res_rewind(rset);
	
   return rset;
}

/*!
 * \fn kd_res_rewind
 * \brief 将指针 rset->riter 指回.
 * \param rset 指向结果链表的指针.
 * \date 2013/08/01
 */
void kd_res_rewind( struct kdres *rset )
{
   rset->riter = rset->rlist->next;
}

/*!
 * \fn kd_res_size
 * \brief 获取查找结果的个数.
 * \param set 指向结果链表的指针.
 * \date 2013/08/01
 */
int kd_res_size( struct kdres *set )
{
   return (set->size);
}

/*!
 * \fn kd_res_free
 * \brief 销毁一个查找结果链表.
 * \param rset 指向待销毁的结果链表的指针.
 * \date 2013/08/01
 */
void kd_res_free( struct kdres *rset )
{
   clear_results(rset);
   free_resnode(rset->rlist);
   free(rset);
}

/*!
 * \fn clear_results
 * \brief 通过循环操作销毁一个查找结果链表中的各个节点.
 * \param rset 指向待销毁的结果链表的指针.
 * \date 2013/08/01
 */
static void clear_results( struct kdres *rset )
{
   struct res_node *tmp, *node = rset->rlist->next;

   while (node) 
   {
      tmp = node;
      node = node->next;
      free_resnode(tmp);
   }

   rset->rlist->next = 0;
}

/*!
 * \fn kd_free
 * \brief 销毁 kd 树.
 * \param tree 指向 kd 树的指针.
 * \date 2013/08/01
 */
void kd_free( struct kdtree *tree )
{
   if(tree) 
   {
      kd_clear(tree);
      free(tree);
   }
}

/*!
 * \fn kd_clear
 * \brief 销毁 kd 树.
 * \param tree 指向 kd 树的指针.
 * \date 2013/08/01
 */
void kd_clear( struct kdtree *tree )
{
   clear_rec(tree->root, tree->destr);
   tree->root = 0;

   if (tree->rect) 
   {
      hyperrect_free(tree->rect);
      tree->rect = 0;
   }
}

/*!
 * \fn clear_rec
 * \brief 释放 kd 树中的所有节点.
 * \param node 指向 kd 树中的当前节点的指针(初始为根节点).
 * \param destr() 由用户提供的销毁函数, 默认为空.
 * \date 2013/08/01
 */ 
static void clear_rec( struct kdnode *node, void (*destr)(void*) )
{
   if (!node) return;

   clear_rec(node->left, destr);
   clear_rec(node->right, destr);
	
   if (destr) 
   {
      destr(node->data);
   }
   free(node->pos);
   free(node);
}

/*!
 * \fn kd_nearest_i
 * \brief 递归查询.
 * \date 2013/08/01
 */
static void 
kd_nearest_i( struct kdnode       *node, 
              const double        *pos, 
              struct kdnode      **result, 
              double              *result_dist_sq, 
              struct kdhyperrect  *rect )
{
   int dir = node->dir;
   int i;
   double dummy, dist_sq;
   struct kdnode *nearer_subtree, *farther_subtree;
   double *nearer_hyperrect_coord, *farther_hyperrect_coord;

   // Decide whether to go left or right in the tree
   dummy = pos[dir] - node->pos[dir];
   if (dummy <= 0) 
   {
      nearer_subtree = node->left;
      farther_subtree = node->right;
      nearer_hyperrect_coord = rect->max + dir;
      farther_hyperrect_coord = rect->min + dir;
   } 
   else 
   {
      nearer_subtree = node->right;
      farther_subtree = node->left;
      nearer_hyperrect_coord = rect->min + dir;
      farther_hyperrect_coord = rect->max + dir;
   }

   if (nearer_subtree) 
   {
      // Slice the hyperrect to get the hyperrect of the nearer subtree
      dummy = *nearer_hyperrect_coord;
      *nearer_hyperrect_coord = node->pos[dir];
      // Recurse down into nearer subtree
      kd_nearest_i(nearer_subtree, pos, result, result_dist_sq, rect);
      // Undo the slice
      *nearer_hyperrect_coord = dummy;
   }

   // Check the distance of the point at the current node, compare it
   // with our best so far
   dist_sq = 0;
   for (i = 0; i < rect->dim; i ++) 
   {
      dist_sq += SQ(node->pos[i] - pos[i]);
   }
   if (dist_sq < *result_dist_sq) 
   {
      *result = node;
      *result_dist_sq = dist_sq;
   }

   if (farther_subtree) 
   {
      // Get the hyperrect of the farther subtree
      dummy = *farther_hyperrect_coord;
      *farther_hyperrect_coord = node->pos[dir];
      // Check if we have to recurse down by calculating the closest
      // point of the hyperrect and see if it's closer than our
      // minimum distance in result_dist_sq.
      if (hyperrect_dist_sq(rect, pos) < *result_dist_sq) 
      {
         // Recurse down into farther subtree
         kd_nearest_i(farther_subtree, pos, result, result_dist_sq, rect);
      }
      // Undo the slice on the hyperrect
      *farther_hyperrect_coord = dummy;
   }
}

/*!
 * \fn kd_nearest
 * \brief 查询 kd 树中与 pos 距离最近的节点.
 * \param kd 指向 kd 树的指针.
 * \param pos 基准节点.
 * \date 2013/08/01
 */
struct kdres *kd_nearest( struct kdtree *kd, const double *pos )
{
   struct kdhyperrect *rect;
   struct kdnode *result;
   struct kdres *rset;
   double dist_sq;
   int i;

   if (!kd) return 0;
   if (!kd->rect) return 0;

   // Allocate result set
   if (!(rset = malloc(sizeof *rset))) 
   {
      return 0;
   }
   
   if ( !(rset->rlist = alloc_resnode()) ) 
   {
      free(rset);
      return 0;
   }
   rset->rlist->next = 0;
   rset->tree = kd;

   // Duplicate the bounding hyperrectangle, we will work on the copy
   if (!(rect = hyperrect_duplicate(kd->rect))) 
   {
      kd_res_free(rset);
      return 0;
   }

   // Our first guesstimate is the root node
   result = kd->root;
   dist_sq = 0;
   for (i = 0; i < kd->dim; i++)
   {
      dist_sq += SQ(result->pos[i] - pos[i]);
   }
   
   // Search for the nearest neighbour recursively
   kd_nearest_i(kd->root, pos, &result, &dist_sq, rect);

   // Free the copy of the hyperrect
   hyperrect_free(rect);

   // Store the result
   if (result) 
   {
      if (rlist_insert(rset->rlist, result, -1.0) == -1) 
      {
         kd_res_free(rset);
         return 0;
      }
      rset->size = 1;
      kd_res_rewind(rset);
      return rset;
   } 
   else 
   {
      kd_res_free(rset);
      return 0;
   }
}
