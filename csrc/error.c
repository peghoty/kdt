/*========================================================================*
 *  KDT (K-d tree analysis) (c) 2013-2014                                 *
 *  peghoty@163.com     (Zhou Zhiyang)                                    *
 *========================================================================*/

#include "utl.h"

/*!
 * \fn void kdt_error_handler
 * \brief Process the error with code ierr raised in the given 
 *        line of the given source file.
 * \date 2013/07/31 
 */  
void 
kdt_error_handler( char *filename, int line, int ierr )
{
   fprintf(stderr, "kdt_error in file \"%s\", line %d, error code = %d\n", filename, line, ierr);
}
