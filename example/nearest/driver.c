/*========================================================================*
 *  KDT (K-d tree analysis) (c) 2013-2014                                 *
 *  peghoty@163.com     (Zhou Zhiyang)                                    *
 *========================================================================*/

/*!
 *  driver.c -- KDT test.
 *
 *  Created by peghoty 2013/08/01
 *
 */  

#include "kdt.h"

int 
main( int argc, char *argv[] )
{
#if 0
   struct timeval starttime, endtime;
   ca_gettime(starttime);
   ca_gettime(endtime);
   printf(">>> time: %.2lf (s)\n", ca_mytime(starttime, endtime));
#endif
   
   struct kdtree *kd;
   struct kdres  *set;

   int i;
   int vcount = 9; // vcount 为向量个数
   
   double xx[9]; 
   double yy[9];
   double buf[2];
	
   xx[0] = 0;
   yy[0] = 0;
	
   xx[1] = 4;
   yy[1] = 0;

   xx[2] = 1;
   yy[2] = 1;

   xx[3] = 3;
   yy[3] = 1;

   xx[4] = 2;
   yy[4] = 2;
   
   xx[5] = 1;
   yy[5] = 3;
   
   xx[6] = 3;
   yy[6] = 3;
   
   xx[7] = 0;
   yy[7] = 4;
   
   xx[8] = 4;
   yy[8] = 4;            
	
   // 创建一个维度为 2 的 kd 树
   kd = kd_create(2);

   for (i = 0; i < vcount; i ++) 
   {
      buf[0] = xx[i];
      buf[1] = yy[i];

      // 将当前向量插入到 kd 树中
      assert(kd_insert(kd, buf, 0) == 0);
   }

   // 查找 kd 树中与 buf 最近的节点
   buf[0] = 2.0;
   buf[1] = 0.0;
   set = kd_nearest(kd, buf); // 只返回一个节点, 即使与 buf 最近邻(距离相等)的节点有多个
 
   // 打印结果
   struct res_node *pt = NULL;
   pt = set->rlist->next;
   printf("(%f,%f)\n", pt->item->pos[0], pt->item->pos[1]);
   printf("dis_sq = %f\n", pt->dist_sq);

   kd_res_free(set);
   kd_free(kd);
	
   return 0;
}
